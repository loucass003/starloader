package api.resource;

import api.DebugFile;
import api.mod.StarLoader;
import api.utils.FilesUtils;
import api.utils.ResourceLocation;
import org.newdawn.slick.ImageBuffer;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.graphicsengine.texture.TextureLoader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BlocksAtlas {

    public HashMap<ResourceLocation, HashMap<BlockTexture.Resolution, BlockTexture>> blocksTexture = new HashMap<>();
    public ArrayList<Texture> altasTextures = new ArrayList<>();
    public ArrayList<Texture> altasTexturesNormals = new ArrayList<>();
    public BufferedImage imageBuff;
    public BufferedImage imageBuffNormals;
    public Graphics2D g2d;
    public Graphics2D g2dNormals;
    public int texturesCount;
    public int totalTextures;

    public void addBlockTexture(ResourceLocation texture_id, BlockTexture texture) {
        HashMap<BlockTexture.Resolution, BlockTexture> current;

        current = blocksTexture.get(texture_id);
        if (current == null)
            current = new HashMap<>();
        current.put(texture.getResolution(), texture);
        blocksTexture.put(texture_id, current);
    }

    public void generateAtlas(BlockTexture.Resolution resolution) {
        texturesCount = 0;
        totalTextures = 0;
        imageBuff = new BufferedImage(resolution.getResolution() * 16, resolution.getResolution() * 16, BufferedImage.TYPE_INT_ARGB);
        imageBuffNormals = new BufferedImage(resolution.getResolution() * 16, resolution.getResolution() * 16, BufferedImage.TYPE_INT_RGB);
        g2d = imageBuff.createGraphics();
        g2d.setColor(new Color(0, 0, 0, 0));
        g2dNormals = imageBuffNormals.createGraphics();
        g2dNormals.setColor(new Color(128, 128, 255, 255));
        g2dNormals.fillRect(0, 0, imageBuffNormals.getWidth(), imageBuffNormals.getHeight());
        for (Map.Entry<ResourceLocation, HashMap<BlockTexture.Resolution, BlockTexture>> entry : blocksTexture.entrySet()) {
            BlockTexture texture = entry.getValue().get(BlockTexture.Resolution.PIXELS128); //TODO: GET CORRESPONDING RESOLUTION
            try {
                DebugFile.log("TEXTURE " + texture.getAmbientTexture());
                InputStream is = FilesUtils.getFileFromMod(
                        StarLoader.getModFromDomain(texture.getAmbientTexture().getResourceDomain()),
                        texture.getAmbientTexture().getResourcePath()
                );
                BufferedImage image = ImageIO.read(is);
                BufferedImage imageNormal = null;
                if (texture.hasNormalMapping()) {
                    is = FilesUtils.getFileFromMod(
                            StarLoader.getModFromDomain(texture.getAmbientTexture().getResourceDomain()),
                            texture.getAmbientTexture().getResourcePath()
                    );
                    imageNormal = ImageIO.read(is);
                }
                addTextureToAtlas(resolution, image, imageNormal);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (texturesCount > 0) {
            createTexture(imageBuff, this.altasTextures);
            createTexture(imageBuffNormals, this.altasTexturesNormals);
        }
        g2d.dispose();
        g2dNormals.dispose();
        imageBuff.flush();
        imageBuffNormals.flush();
    }

    public void createTexture(BufferedImage image, ArrayList<Texture> textures)
    {
        textures.add(TextureLoader.getTexture(image, "atlas_" + totalTextures, 3553, 6408, 9729, 9729, true, true));
        totalTextures++;
    }

    public void addTextureToAtlas(BlockTexture.Resolution resolution, BufferedImage image, BufferedImage imageNormals)
    {
        if (texturesCount == 256 + 1) {
            createTexture(imageBuff, this.altasTextures);
            createTexture(imageBuffNormals, this.altasTexturesNormals);

            g2d.fillRect(0, 0, imageBuff.getWidth(), imageBuff.getHeight());
            g2dNormals.fillRect(0, 0, imageBuff.getWidth(), imageBuff.getHeight());
            texturesCount = 0;
        }
        g2d.drawImage(image,
            (texturesCount % 16) * resolution.getResolution(),
            (texturesCount / 16) * resolution.getResolution(),
            (texturesCount % 16) * resolution.getResolution() + resolution.getResolution(),
            (texturesCount / 16) * resolution.getResolution() + resolution.getResolution(),
            0, 0, image.getWidth(), image.getHeight(), null);
        if (imageNormals != null)
            g2dNormals.drawImage(imageNormals,
                    (texturesCount % 16) * resolution.getResolution(),
                    (texturesCount / 16) * resolution.getResolution(),
                    (texturesCount % 16) * resolution.getResolution() + resolution.getResolution(),
                    (texturesCount / 16) * resolution.getResolution() + resolution.getResolution(),
                    0, 0, imageNormals.getWidth(), imageNormals.getHeight(), null);
        texturesCount++;
    }

    public int countNeededTextures()
    {
        return (blocksTexture.size() / 256) + 1;
    }

    public ArrayList<Texture> getAltasTextures() {
        return altasTextures;
    }

    public ArrayList<Texture> getAltasTexturesNormals() {
        return altasTexturesNormals;
    }
}
