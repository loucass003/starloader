package api.resource;

import api.utils.ResourceLocation;

public class BlockTexture {

    public enum Resolution {
        PIXELS64(64),
        PIXELS128(128),
        PIXELS256(256),
        PIXELS512(512);

        private final int resolution;

        Resolution(int resolution) {
            this.resolution = resolution;
        }

        public int getResolution() {
            return resolution;
        }

        public static Resolution getCloseResolution(int res)
        {
            Resolution r = PIXELS64;

            int distance = Math.abs(PIXELS64.getResolution() - res);
            for (int c = 1; c < Resolution.values().length; c++){
                int cdistance = Math.abs(Resolution.values()[c].getResolution() - res);
                if(cdistance < distance){
                    r = Resolution.values()[c];
                    distance = cdistance;
                }
            }
            return r;
        }
    }

    private Resolution resolution;
    private boolean hasNormalMapping;
    private ResourceLocation ambientTexture;
    private ResourceLocation normalTexture;

    public BlockTexture(Resolution resolution, ResourceLocation ambientTexture) {
        this(resolution, ambientTexture, null);
    }

    public BlockTexture(Resolution resolution, ResourceLocation ambientTexture, ResourceLocation normalTexture) {
        this.resolution = resolution;
        this.ambientTexture = ambientTexture;
        this.normalTexture = normalTexture;
        hasNormalMapping = this.normalTexture != null;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public boolean hasNormalMapping() {
        return hasNormalMapping;
    }

    public ResourceLocation getAmbientTexture() {
        return ambientTexture;
    }

    public ResourceLocation getNormalTexture() {
        return normalTexture;
    }
}
