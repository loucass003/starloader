package api.resource;

import api.DebugFile;
import api.mod.StarLoader;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.ResourceLoadEntry;
import org.schema.schine.resource.ResourceLoader;

public class BlockResourceLoaderEntry extends ResourceLoadEntry {

    public BlockResourceLoaderEntry(String s, int i) {
        super(s, i);
    }

    public final boolean canLoad() {
        return !GraphicsContext.isTextureArrayEnabled();
    }

    public final void load(ResourceLoader var1x) throws ResourceException {
        System.err.println("[RESOURCE][Starloader] loding starloader blocks textures");
        DebugFile.log("loding starloader blocks textures");
        int res = (Integer)EngineSettings.G_TEXTURE_PACK_RESOLUTION.getCurrentState();
        BlocksAtlas atlas = StarLoader.getResourcesRegistry().getBlocksAtlas();
        atlas.generateAtlas(BlockTexture.Resolution.getCloseResolution(res));
        for (int i = 0; i < atlas.countNeededTextures(); i++) {
            GameResourceLoader.cubeTextures[8 + i] = atlas.getAltasTextures().get(i);
            GameResourceLoader.cubeTexturesLow[8 + i] = atlas.getAltasTextures().get(i);
            GameResourceLoader.cubeNormalTextures[8 + i] = atlas.getAltasTexturesNormals().get(i);
            GameResourceLoader.cubeNormalTexturesLow[8 + i] = atlas.getAltasTexturesNormals().get(i);
        }
        System.out.println(GameResourceLoader.cubeTextures);
    }
}
