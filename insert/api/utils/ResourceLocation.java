package api.utils;

import api.mod.StarMod;
import org.apache.commons.lang3.Validate;

import java.util.Locale;

public class ResourceLocation implements Comparable<ResourceLocation>
{
    protected final String resourceDomain;
    protected final String resourcePath;

    public ResourceLocation(String resourceDomainIn, String resourcePathIn)  {
        this.resourceDomain = resourceDomainIn.toLowerCase(Locale.ROOT);
        this.resourcePath = resourcePathIn.toLowerCase(Locale.ROOT);
        Validate.notNull(this.resourcePath);
    }

    public ResourceLocation(StarMod mod, String resourcePathIn)  {
        this(mod.modName, resourcePathIn);
    }

    public String getResourcePath() {
        return this.resourcePath;
    }

    public String getResourceDomain()  {
        return this.resourceDomain;
    }

    public String toString()  {
        return this.resourceDomain + ':' + this.resourcePath;
    }

    public boolean equals(Object p_equals_1_)  {
        if (this == p_equals_1_)
            return true;
        else if (!(p_equals_1_ instanceof ResourceLocation))
            return false;
        else
        {
            ResourceLocation resourcelocation = (ResourceLocation)p_equals_1_;
            return this.resourceDomain.equals(resourcelocation.resourceDomain) && this.resourcePath.equals(resourcelocation.resourcePath);
        }
    }

    public int hashCode()
    {
        return 31 * this.resourceDomain.hashCode() + this.resourcePath.hashCode();
    }

    public int compareTo(ResourceLocation p_compareTo_1_)
    {
        int i = this.resourceDomain.compareTo(p_compareTo_1_.resourceDomain);

        if (i == 0)
        {
            i = this.resourcePath.compareTo(p_compareTo_1_.resourcePath);
        }

        return i;
    }

}