package api.utils;

import api.mod.StarMod;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FilesUtils {

    public static InputStream getFileFromMod(StarMod mod, String path)
    {
        return mod.getClass().getResourceAsStream("/" + path);
    }

}
