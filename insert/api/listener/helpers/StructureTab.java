package api.listener.helpers;

/**
 * All of the advanced build mode structure tabs.
 */
public enum StructureTab {
    STATS,
    DOCKS,
    FACTION,
    GENERAL,
    GUISGROUP, //Some sort of base for all the other tabs?
    HELP_TOP,
    POWER,
    SHIELD,
    STRUCTURE,
    THRUSTER,
    WEAPONS
}
